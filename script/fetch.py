import requests
import sys
import os
import re
from urllib.parse import urlparse, urljoin
import datetime
from bs4 import BeautifulSoup

userAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36"

def main(urls):
    headers = {"User-Agent": userAgent}
    metadataFlag = "--metadata" in urls
    for url in urls:
        if "--metadata" == url:
            continue
        if "http" not in url:
            print("Invalid URL: %s"%url)
            print("Trying https://%s ..."%url)
            url = "https://%s"%url

        parsedUrl = urlparse(url)
        domain = parsedUrl.netloc

        # check domain name
        if not domain or domain == '':
            print("Invalid URL: %s"%url)
            continue
        try:
            print("Fetching %s ..."%url)
            response = requests.get(url, headers=headers, allow_redirects=True, timeout=6)
            # Successful responses (200–299)
            if response.status_code > 199 and response.status_code < 300:
                url = response.url
                domain = urlparse(url).netloc
                html = downloadAssets(domain, response.text, url, metadataFlag)
                print("Done!")
                makeHtml(domain, html)
            elif response.status_code == 404:
                print("Invalid URL: %s"%url)
            else:
                print("Protected URL: %s"%url)
        except:
            print("Request Timeout!")

def downloadAssets(domain, html, url, metadataFlag):
    checkDirectory(domain)
    #check js assets directory
    checkDirectory(domain + "/js")
    #check css assets directory
    checkDirectory(domain + "/css")
    #check images assets directory
    checkDirectory(domain + "/images")
    soup = BeautifulSoup(html, 'html.parser')

    links = soup.find_all('link')
    images = soup.find_all('img')
    hyperlinks = soup.find_all('a')
    if metadataFlag:
        print("-"*50)
        print("site: %s" % domain)
        print("num_links: %d" % len(hyperlinks))
        print("images: %d" % len(images))
        today = datetime.datetime.now()
        print("last_fetch: %s" % today.strftime("%a %b %d %Y %H:%M %Z"))
        print("-"*50)

    if links and len(links)>0:
        print("Downloading css files ...")
        for link in links:
            try:
                cssUrl = urljoin(url, link['href'])
                if link.get('as') and link.get('as') == 'script':
                    localCSSUrl = downloadFileFromUrl(cssUrl, domain + "/js/")
                    link['href'] = localCSSUrl

                if link.get('rel') and link['rel'][0] == 'stylesheet':
                    localCSSUrl = downloadFileFromUrl(cssUrl, domain + "/css/")
                    link['href'] = localCSSUrl
            except:
                pass

    scripts = soup.find_all('script')
    if scripts and len(scripts)>0:
        print("Downloading script files ...")
        for script in scripts:
            try:
                scriptUrl = urljoin(url, script['src'])
                # localJSUrl = downloadFileFromUrl(scriptUrl, domain + "/js/")
                script['src'] = scriptUrl
            except:
                pass

    if images and len(images)>0:
        print("Downloading images files ...")
        for image in images:
            if "data:image" in image['src'] or image['src']=='':
                continue
            imgUrl = urljoin(url, image['src'])
            localImgUrl = downloadFileFromUrl(imgUrl, domain + "/images/")
            image['src'] = localImgUrl
    return soup.prettify()

def checkDirectory(path):
    isExist = os.path.exists(path)
    # Create a new directory if it does not exist 
    if not isExist:
        os.makedirs(path)

def makeHtml(path, html):
    fname = path + ".html"
    f = open(fname, "w")
    f.write(html)
    f.close()

def getFilename_fromCd(cd):
    # Get filename from content-disposition
    if not cd:
        return None
    fname = re.findall('filename=(.+)', cd)
    if len(fname) == 0:
        return None
    return fname[0]

def downloadFileFromUrl(url, path):
    try:
        r = requests.get(url, allow_redirects=True, timeout=6)
        fname = getFilename_fromCd(r.headers.get('content-disposition'))
        if fname is None:
            fname = url.split("/")[-1]
            fname = re.sub(r'\?.*', '', fname)

        filename = path + fname
        if os.path.isfile(path):
            return filename
        f = open(filename, 'wb')
        f.write(r.content)
        f.close()
        return filename
    except:
        return url

if __name__ == '__main__':
    if sys.argv is None or len(sys.argv) < 2:
        print("Usage: ./fetch [--metadata] URL1, URL2, ...\n")
    else:
        urls = sys.argv[1:]
        main(urls)