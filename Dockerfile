FROM python:3.8

WORKDIR /task

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY ./script ./script

WORKDIR /task/script
